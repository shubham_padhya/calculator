/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package calculater;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class Add extends Calculator {

    public Add(double operand1, double operand2) {
        super(operand1, operand2);
    }
    
    public double Calculate(){
    return operand1 + operand2 ;
    } 

   
}